package com.atlassian.johnson.spring.lifecycle;

import com.google.common.base.MoreObjects;

import javax.annotation.Nonnull;
import javax.servlet.ServletContext;

/**
 * @since 3.0
 */
public class LifecycleUtils {

    private static final String ATTR_STATE = LifecycleState.class.getName() + ":Current";

    private LifecycleUtils() {
        throw new UnsupportedOperationException(getClass() + " is a utility class and should not be instantiated.");
    }

    @Nonnull
    public static LifecycleState getCurrentState(@Nonnull ServletContext servletContext) {
        LifecycleState state = (LifecycleState) servletContext.getAttribute(ATTR_STATE);

        return MoreObjects.firstNonNull(state, LifecycleState.CREATED);
    }

    public static boolean isStarting(@Nonnull ServletContext servletContext) {
        LifecycleState state = getCurrentState(servletContext);

        return state == LifecycleState.CREATED || state == LifecycleState.STARTING;
    }

    public static void updateState(@Nonnull ServletContext servletContext, @Nonnull LifecycleState state) {
        servletContext.setAttribute(ATTR_STATE, state);
    }
}
