package com.atlassian.johnson.filters;

import com.atlassian.johnson.DefaultJohnsonEventContainer;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class Johnson503FilterTest {

    private static final EventType DEFAULT_EVENT_TYPE = new EventType("event-type", "event-type-desc");

    @Mock
    private HttpServletResponse response;
    @Mock
    private PrintWriter writer;

    @Before
    public void setup() throws IOException {
        when(response.getWriter()).thenReturn(writer);
    }

    @Test
    public void testHandleErrorWithNullContainer() throws IOException {
        new Johnson503Filter().handleError(null, null, response);

        verify(response).setStatus(eq(HttpServletResponse.SC_SERVICE_UNAVAILABLE));
        verify(response, never()).setHeader(anyString(), anyString());
        verify(response).getWriter();
        verify(writer).flush();
    }

    @Test
    public void testHandleErrorWithEmptyContainer() throws IOException {
        new Johnson503Filter().handleError(createContainer(), null, response);

        verify(response).setStatus(eq(HttpServletResponse.SC_SERVICE_UNAVAILABLE));
        verify(response).setHeader(eq("Retry-After"), eq("30"));
        verify(response).getWriter();
        verify(writer).flush();
    }

    @Test
    public void testHandleErrorWithWarningOnly() throws IOException {
        new Johnson503Filter().handleError(createContainer(EventLevel.WARNING), null, response);

        verify(response).setStatus(eq(HttpServletResponse.SC_SERVICE_UNAVAILABLE));
        verify(response).setHeader(eq("Retry-After"), eq("30"));
        verify(writer).flush();
    }

    @Test
    public void testHandleErrorWithErrorOnly() throws IOException {
        new Johnson503Filter().handleError(createContainer(EventLevel.ERROR), null, response);

        verify(response).setStatus(eq(HttpServletResponse.SC_SERVICE_UNAVAILABLE));
        verify(response, never()).setHeader(anyString(), anyString());
        verify(writer).flush();
    }

    @Test
    public void testHandleErrorWithCustomLevel() throws IOException {
        new Johnson503Filter().handleError(createContainer("my-level"), null, response);

        verify(response).setStatus(eq(HttpServletResponse.SC_SERVICE_UNAVAILABLE));
        verify(response, never()).setHeader(anyString(), anyString());
        verify(writer).flush();
    }

    @Test
    public void testHandleErrorWithErrorAndWarning() throws IOException {
        new Johnson503Filter().handleError(createContainer(EventLevel.ERROR, EventLevel.WARNING), null, response);

        verify(response).setStatus(eq(HttpServletResponse.SC_SERVICE_UNAVAILABLE));
        verify(response, never()).setHeader(anyString(), anyString());
        verify(writer).flush();
    }

    @Test
    public void testHandleNotSetup() throws IOException {
        new Johnson503Filter().handleNotSetup(null, response);

        verify(response).setStatus(eq(HttpServletResponse.SC_SERVICE_UNAVAILABLE));
        verify(response, never()).setHeader(anyString(), anyString());
        verify(writer).flush();
    }

    private static JohnsonEventContainer createContainer(String... levels) {
        JohnsonEventContainer container = new DefaultJohnsonEventContainer();

        Stream.of(levels)
                .map(Johnson503FilterTest::createEvent)
                .forEach(container::addEvent);

        return container;
    }

    private static Event createEvent(String eventLevel) {
        return new Event(DEFAULT_EVENT_TYPE, "event-desc", createEventLevel(eventLevel));
    }

    private static EventLevel createEventLevel(String eventLevel) {
        return new EventLevel(eventLevel, "event-level-desc");
    }
}
