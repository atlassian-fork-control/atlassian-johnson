package com.atlassian.johnson.event;

import javax.annotation.Nullable;

import static com.atlassian.johnson.event.EventLevel.*;

/**
 * Commonly-used {@link EventLevel}s. This is more typesafe than using the Strings in that class.
 * <p>
 * This is not an enum because the levels can't be loaded until Johnson is initialised.
 *
 * @since 3.2
 */
public final class EventLevels {

    private EventLevels() {
        throw new UnsupportedOperationException(getClass().getSimpleName() + " should not be instantiated");
    }

    /**
     * Returns the {@link EventLevel#ERROR} event level, if it has been defined.
     *
     * @return the {@link EventLevel#ERROR} level, or {@code null} if the application does not define it
     * @throws IllegalStateException if Johnson has not been initialised
     */
    @Nullable
    public static EventLevel error() {
        return get(ERROR);
    }

    /**
     * Returns the {@link EventLevel#FATAL} event level, if it has been defined.
     *
     * @return the {@link EventLevel#FATAL} level, or {@code null} if the application does not define it
     * @throws IllegalStateException if Johnson has not been initialised
     */
    @Nullable
    public static EventLevel fatal() {
        return get(FATAL);
    }

    /**
     * Returns the {@link EventLevel#WARNING} event level, if it has been defined.
     *
     * @return the {@link EventLevel#WARNING} level, or {@code null} if the application does not define it
     * @throws IllegalStateException if Johnson has not been initialised
     */
    @Nullable
    public static EventLevel warning() {
        return get(WARNING);
    }
}
