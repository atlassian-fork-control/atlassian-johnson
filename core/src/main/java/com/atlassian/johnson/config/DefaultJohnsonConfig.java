package com.atlassian.johnson.config;

import com.atlassian.johnson.event.*;
import com.atlassian.johnson.setup.ContainerFactory;
import com.atlassian.johnson.setup.DefaultContainerFactory;
import com.atlassian.johnson.setup.DefaultSetupConfig;
import com.atlassian.johnson.setup.SetupConfig;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A default implementation of {@link JohnsonConfig} which may be used as a failsafe when no other configuration is
 * available.
 * <p>
 * All URIs are {@link #isIgnoredPath(String) ignored} by this implementation. All collection-returning properties are
 * implemented to return empty, immutable collections. All other methods generally return {@code null}, except for:
 * <ul>
 * <li>{@link SetupConfig} is provided by {@link DefaultSetupConfig}</li>
 * <li>{@link ContainerFactory} is provided by {@link DefaultContainerFactory}</li>
 * </ul>
 * This class cannot be instantiated. A single, immutable instance is available via {@link #getInstance()}.
 *
 * @since 2.0
 */
public final class DefaultJohnsonConfig implements JohnsonConfig {

    private static final DefaultJohnsonConfig instance = new DefaultJohnsonConfig();

    private final ContainerFactory containerFactory;
    private final SetupConfig setupConfig;

    private DefaultJohnsonConfig() {
        containerFactory = new DefaultContainerFactory();
        setupConfig = new DefaultSetupConfig();
    }

    /**
     * Retrieves the immutable singleton instance of the default configuration.
     *
     * @return the default configuration singleton
     */
    @Nonnull
    public static JohnsonConfig getInstance() {
        return instance;
    }

    /**
     * Always empty but non-{@code null}.
     *
     * @return an empty list
     */
    @Nonnull
    @Override
    public List<ApplicationEventCheck> getApplicationEventChecks() {
        return Collections.emptyList();
    }

    /**
     * Always an instance of {@link DefaultContainerFactory}.
     *
     * @return a default container factory
     */
    @Nonnull
    @Override
    public ContainerFactory getContainerFactory() {
        return containerFactory;
    }

    /**
     * Always {@code "/unavailable"}. {@link #isIgnoredPath(String)} always returns {@code true}, so this path
     * should never be accessed; Johnson processing is bypassed for all paths.
     *
     * @return {@code "/unavailable"}
     */
    @Nonnull
    @Override
    public String getErrorPath() {
        return "/unavailable";
    }

    /**
     * Always {@code null}.
     *
     * @param id ignored
     * @return {@code null}
     */
    @Override
    public EventCheck getEventCheck(int id) {
        return null;
    }

    /**
     * Always empty but non-{@code null}.
     *
     * @return an empty list
     */
    @Nonnull
    @Override
    public List<EventCheck> getEventChecks() {
        return Collections.emptyList();
    }

    /**
     * Always {@code null}.
     *
     * @param level ignored
     * @return {@code null}
     */
    @Override
    public EventLevel getEventLevel(@Nonnull String level) {
        checkNotNull(level, "level");

        return null;
    }

    /**
     * Always {@code null}.
     *
     * @param type ignored
     * @return {@code null}
     */
    @Override
    public EventType getEventType(@Nonnull String type) {
        checkNotNull(type, "type");

        return null;
    }

    /**
     * Always empty but non-{@code null}.
     *
     * @return an empty list
     */
    @Nonnull
    @Override
    public List<String> getIgnorePaths() {
        return Collections.emptyList();
    }

    /**
     * Always empty but non-{@code null}.
     *
     * @return an empty map
     */
    @Nonnull
    @Override
    public Map<String, String> getParams() {
        return Collections.emptyMap();
    }

    /**
     * Always empty but non-{@code null}.
     *
     * @return an empty list
     */
    @Nonnull
    @Override
    public List<RequestEventCheck> getRequestEventChecks() {
        return Collections.emptyList();
    }

    /**
     * Always an instance of {@link DefaultSetupConfig}.
     */
    @Nonnull
    @Override
    public SetupConfig getSetupConfig() {
        return setupConfig;
    }

    /**
     * Always {@code "/setup"}. The default configuration is always setup, so this path should never be accessed.
     *
     * @return {@code "/setup"}
     */
    @Nonnull
    @Override
    public String getSetupPath() {
        return "/setup";
    }

    /**
     * Always {@code true}. This has the net effect of disabling Johnson, because all URIs included for filtering will
     * be ignored and events will not be processed for them.
     *
     * @param uri ignored
     * @return {@code true}
     */
    @Override
    public boolean isIgnoredPath(@Nonnull String uri) {
        checkNotNull(uri, "uri");

        return true;
    }
}
