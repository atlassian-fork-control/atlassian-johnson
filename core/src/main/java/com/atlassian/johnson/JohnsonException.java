package com.atlassian.johnson;

/**
 * Base class for all exceptions thrown by Johnson.
 * <p>
 * Note: This base class explicitly disallows the construction of exceptions without a message, a throwable or both.
 *
 * @since 2.0
 */
public class JohnsonException extends RuntimeException {

    /**
     * Constructs a new {@code JohnsonException} with the provided message.
     *
     * @param s the exception message
     */
    public JohnsonException(String s) {
        super(s);
    }

    /**
     * Constructs a new {@code JohnsonException} with the provided message and cause.
     *
     * @param s         the exception message
     * @param throwable the cause
     */
    public JohnsonException(String s, Throwable throwable) {
        super(s, throwable);
    }

    /**
     * Constructs a new {@code JohnsonException} the the provided cause and no message.
     *
     * @param throwable the cause
     */
    public JohnsonException(Throwable throwable) {
        super(throwable);
    }
}
